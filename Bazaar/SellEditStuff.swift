/*==================================================
 Bazaar
 
 © XScoder 2018
 All Rights reserved
 
 /*
 RE-SELLING THIS SOURCE CODE TO ANY ONLINE MARKETPLACE IS A SERIOUS COPYRIGHT INFRINGEMENT.
 YOU WILL BE LEGALLY PROSECUTED
 */
===================================================*/


import UIKit
import Parse
import CoreLocation

class SellEditStuff: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, CLLocationManagerDelegate {

    /*--- VIEWS ---*/
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var photoButtonsScrollView: UIScrollView!
    @IBOutlet weak var photo1Button: UIButton!
    @IBOutlet weak var photo2Button: UIButton!
    @IBOutlet weak var photo3Button: UIButton!
    @IBOutlet weak var photo4Button: UIButton!
    @IBOutlet weak var photo5Button: UIButton!
    @IBOutlet weak var adTitleTxt: UITextField!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var adPriceTxt: UITextField!
    @IBOutlet weak var negotiableSwitch: UISwitch!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var markAsSoldButton: UIButton!
    @IBOutlet weak var deleteItemButton: UIButton!
    @IBOutlet weak var sellButton: UIButton!
    
    
    
    
    /*--- VARIABLES ---*/
    var adObj = PFObject(className: ADS_CLASS_NAME)
    var isFirstPhoto = Bool()
    var photoButtons = [UIButton]()
    var photoID = 0
    var isNegotiable = true
    var photoAttachedIDs = [Int]()
    var category = ""
    var isSold = false
    var locationManager: CLLocationManager!
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID APPEAR
    // ------------------------------------------------
    override func viewDidAppear(_ animated: Bool) {
        print("AD OBJECT ID: \(String(describing: adObj.objectId))")
        print("PHOTO ID: \(String(describing: photoID))")
        print("IS FIRST PHOTO: \(String(describing: isFirstPhoto))")
        print("CHOSEN LOCATION: \(String(describing: chosenLocation))")

        
        // Automatically dismiss the controller
        if mustDismiss {
            mustDismiss = false
            dismiss(animated: false, completion: nil)
        }
        
        // Item's Location
        if chosenLocation == nil { getCurrentLocation()
        } else { setAdLocation() }
        
        
        // ------------------------------------------------
        // MARK: - NEW ITEM
        // ------------------------------------------------
        if adObj.objectId == nil {
            titleLabel.text = "Sell stuff"

            // Open camera automatically - In case of 1st Photo
            if isFirstPhoto {
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera;
                    imagePicker.allowsEditing = false
                    present(imagePicker, animated: true, completion: nil)
                }
            }
        }
        
        
        
        
        // Set photo Buttons after attaching an image
        switch photoID {
            case 0:
                if !photoAttachedIDs.contains(photoID+1) {
                    photo2Button.setImage(UIImage(named: "add_photo_butt"), for: .normal)
                    photo2Button.backgroundColor = UIColor.white
                }
            break
            case 1:
                if !photoAttachedIDs.contains(photoID+1) {
                    photo3Button.setImage(UIImage(named: "add_photo_butt"), for: .normal)
                    photo3Button.backgroundColor = UIColor.white
                    photo3Button.isEnabled = true
                }
            break
            case 2:
                if !photoAttachedIDs.contains(photoID+1) {
                    photo4Button.setImage(UIImage(named: "add_photo_butt"), for: .normal)
                    photo4Button.backgroundColor = UIColor.white
                    photo4Button.isEnabled = true
                }
            break
            case 3:
                if !photoAttachedIDs.contains(photoID+1) {
                    photo5Button.setImage(UIImage(named: "add_photo_butt"), for: .normal)
                    photo5Button.backgroundColor = UIColor.white
                    photo5Button.isEnabled = true
                }
            break
            
        default:break }
        
        
    }// ./ viewDidAppear
    
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - VIEW DID LOAD
    // ------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Layouts
        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width,
                                                 height: 1200)
        markAsSoldButton.layer.cornerRadius = 22
        deleteItemButton.layer.cornerRadius = 22

        
        // Show Currency code
        priceLabel.text = "PRICE - \(CURRENCY_CODE)"
        
    
        // Array of photo Buttons
        photoButtons = [
            photo1Button,
            photo2Button,
            photo3Button,
            photo4Button,
            photo5Button
        ]
        
        // Setup photo Buttons
        for i in 0..<photoButtons.count {
            let butt = photoButtons[i]
            butt.layer.cornerRadius = 6
            butt.tag = i
            butt.clipsToBounds = true
            butt.imageView?.contentMode = .scaleAspectFill
            if i > 1 { butt.isEnabled = false }
        }
        photoButtonsScrollView.contentSize = CGSize(width: (photo1Button.frame.size.width+20) * 5, height: photoButtonsScrollView.frame.size.height)

        // ------------------------------------------------
        // MARK: - EDIT ITEM
        // ------------------------------------------------
        if adObj.objectId != nil {
            titleLabel.text = "Edit item"
            sellButton.setTitle("Update", for: .normal)
            markAsSoldButton.isHidden = false
            deleteItemButton.isHidden = false
            // Call function
            showItemDetails()
        }
        
        
    }// ./ viewDidLoad
    
    
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - GET CURRENT LOCATION
    // ------------------------------------------------
    func getCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        if locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - CORE LOCATION DELEGATES
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        simpleAlert("Cannot get your Location. Please go into Settings, search this app and enable Location service, so you'll be able to see ads nearby you. Otherwise the app will display ads from New York, USA")
        
        // Set New York City as default currentLocation
        chosenLocation = DEFAULT_LOCATION
        
        // Set distance and city labels
        self.locationButton.setTitle("New York, USA", for: .normal)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        chosenLocation = locations.last!
        locationManager = nil
        
        // Call function
        setAdLocation()
    }
    
    

    
    // ------------------------------------------------
    // MARK: - SET AD LOCATION
    // ------------------------------------------------
    func setAdLocation() {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(chosenLocation!, completionHandler: { (placemarks, error) -> Void in
            let placeArray:[CLPlacemark] = placemarks!
            var placemark: CLPlacemark!
            placemark = placeArray[0]
            let city = placemark.addressDictionary?["City"] as? String ?? ""
            let country = placemark.addressDictionary?["Country"] as? String ?? ""
            // Set Location Button
            self.locationButton.setTitle("   \(city), \(country)", for: .normal)
        })
    }
    
    
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - SHOW ITEM'S DETAILS
    // ------------------------------------------------
    func showItemDetails() {
        // Photo 1
        getParseImage(object: adObj, colName: ADS_IMAGE1, button: photo1Button)
        photoAttachedIDs.append(0)
        
        // Photo 2
        if adObj[ADS_IMAGE2] != nil {
            getParseImage(object: adObj, colName: ADS_IMAGE2, button: photo2Button)
            photoAttachedIDs.append(1)
            photo2Button.isEnabled = true
        }
        // Photo 3
        if adObj[ADS_IMAGE3] != nil {
            getParseImage(object: adObj, colName: ADS_IMAGE3, button: photo3Button)
            photoAttachedIDs.append(2)
            photo3Button.isEnabled = true
        }
        // Photo 4
        if adObj[ADS_IMAGE4] != nil {
            getParseImage(object: adObj, colName: ADS_IMAGE4, button: photo4Button)
            photoAttachedIDs.append(3)
            photo4Button.isEnabled = true
        }
        // Photo 5
        if adObj[ADS_IMAGE5] != nil {
            getParseImage(object: adObj, colName: ADS_IMAGE5, button: photo5Button)
            photoAttachedIDs.append(4)
            photo5Button.isEnabled = true
        }
        print("PHOTO ATTACHED IDs - ON SHOW ITEM'S DETAILS: \(photoAttachedIDs)")

        // Title
        adTitleTxt.text = "\(adObj[ADS_TITLE]!)"
        // Price
        adPriceTxt.text = "\(adObj[ADS_PRICE]!)"
        // Negotiable
        isNegotiable = adObj[ADS_IS_NEGOTIABLE] as! Bool
        if isNegotiable { negotiableSwitch.setOn(true, animated: true)
        } else { negotiableSwitch.setOn(false, animated: true) }
        // Description
        descriptionTxt.text = "\(adObj[ADS_DESCRIPTION]!)"
        // Location
        let adLocation = adObj[ADS_LOCATION] as! PFGeoPoint
        chosenLocation = CLLocation(latitude: adLocation.latitude, longitude: adLocation.longitude)
        setAdLocation()
        // Category
        category = "\(adObj[ADS_CATEGORY]!)"
        categoryButton.setTitle("   " + category, for: .normal)
        // Mark as Sold Button
        let isSold = adObj[ADS_IS_SOLD] as! Bool
        if isSold { markAsSoldButton.isHidden = true
        } else { markAsSoldButton.isHidden = false }
        // Delete Button
        deleteItemButton.isHidden = false
    }
    
    
    
    
    
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - PHOTO BUTTONS
    // ------------------------------------------------
    @IBAction func photoButt(_ sender: UIButton) {
        // Set photoID
        photoID = sender.tag
        
        // Fire alert
        let alert = UIAlertController(title: APP_NAME,
            message: "Select source",
            preferredStyle: .actionSheet)
        
        
        // OPEN CAMERA
        let camera = UIAlertAction(title: "Take a Photo", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        alert.addAction(camera)

        
        // OPEN PHOTO LIBRARY
        let library = UIAlertAction(title: "Choose Image from Library", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        alert.addAction(library)

        
        // CANCEL BUTTON
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            // iPad
            alert.modalPresentationStyle = .popover
            alert.popoverPresentationController?.sourceView = view
            alert.popoverPresentationController?.sourceRect = sender.frame
            present(alert, animated: true, completion: nil)
        } else {
            // iPhone
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - IMAGE PICKER DELEGATE
    // ------------------------------------------------
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // Scale image
            let scaledImg = scaleImageToMaxWidth(image: image, newWidth: 640)
            
            if photoID == 0 { isFirstPhoto = false }
            photoButtons[photoID].setImage(scaledImg, for: .normal)
            
            if photoAttachedIDs.contains(photoID) {
                photoAttachedIDs.remove(at: photoID)
                photoAttachedIDs.insert(photoID, at: photoID)
            } else { photoAttachedIDs.append(photoID) }
            print("PHOTO ATTACHED ID's: \(photoAttachedIDs)")
        }
        dismiss(animated: true, completion: nil)
    }
    
    // Dismiss imagePicker on Cancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if isFirstPhoto {
            isFirstPhoto = false
            mustDismiss = true
            dismiss(animated: true, completion: nil)
        } else {
            photoID -= 1
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - SET NEGOTIABLE PRICE
    // ------------------------------------------------
    @IBAction func negotiableSwitchChanged(_ sender: UISwitch) {
        isNegotiable = !isNegotiable
        print("IS NEGOTIABLE: \(isNegotiable)")
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - LOCATION BUTTON
    // ------------------------------------------------
    @IBAction func locationButt(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MapScreen") as! MapScreen
        vc.isSelling = true
        present(vc, animated: true, completion: nil)
    }
    
    
    
    
    // ------------------------------------------------
    // MARK: - CATEGORY BUTTON
    // ------------------------------------------------
    @IBAction func categoryButt(_ sender: UIButton) {
        let alert = UIAlertController(title: APP_NAME,
            message: "Select a Category",
            preferredStyle: .actionSheet)
        
        var action = UIAlertAction()
        for i in 0..<categoriesArray.count {
            action = UIAlertAction(title: "\(categoriesArray[i])", style: .default, handler: { (action) -> Void in
                self.category = categoriesArray[i]
                self.categoryButton.setTitle("   " + self.category, for: .normal)
            })
            alert.addAction(action)
        }
        
        // Cancel button
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            // iPad
            alert.modalPresentationStyle = .popover
            alert.popoverPresentationController?.sourceView = view
            alert.popoverPresentationController?.sourceRect = sender.frame
            present(alert, animated: true, completion: nil)
        } else {
            // iPhone
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - SELL BUTTON
    // ------------------------------------------------
    @IBAction func sellButt(_ sender: Any) {
        let currentUser = PFUser.current()!
        
        // Missing info
        if adTitleTxt.text == "" || adPriceTxt.text == "" || descriptionTxt.text == "" ||
            category == "" {
            simpleAlert("Please make sure you have inserted the following info:\n• Title\n• Price\n• Description\n• Category")
            
            
        // All info have been set -> save data
        } else {
            showHUD()
            dismisskeyboard()
            
            // Prepare data for a New Item
            if adObj.objectId == nil {
                adObj[ADS_LIKED_BY] = [String]()
                adObj[ADS_FOLLOWED_BY] = [String]()
                adObj[ADS_LIKES] = 0
                adObj[ADS_VIEWS] = 0
                adObj[ADS_IS_REPORTED] = false
            }
            
            // Prepare general data
            adObj[ADS_SELLER_POINTER] = currentUser
            adObj[ADS_TITLE] = adTitleTxt.text!
            adObj[ADS_CATEGORY] = category
            adObj[ADS_PRICE] = Int(adPriceTxt.text!)
            adObj[ADS_CURRENCY] = CURRENCY_CODE
            adObj[ADS_LOCATION] = PFGeoPoint(latitude: chosenLocation!.coordinate.latitude, longitude: chosenLocation!.coordinate.longitude)
            adObj[ADS_DESCRIPTION] = descriptionTxt.text!
            adObj[ADS_IS_NEGOTIABLE] = isNegotiable
            adObj[ADS_IS_SOLD] = isSold
            
            // Prepare keywords
            let k1NoCommas = adTitleTxt.text!.lowercased().replacingOccurrences(of: ",", with: " ")
            let k2NoCommas = descriptionTxt.text!.lowercased().replacingOccurrences(of: ",", with: " ")
            let k1 = k1NoCommas.components(separatedBy: " ")
            let k2 = k2NoCommas.components(separatedBy: " ")
            let k3 = "\(currentUser[USER_USERNAME]!)".lowercased().components(separatedBy: " ")
            let keywords = k1 + k2 + k3
            adObj[ADS_KEYWORDS] = keywords
            
            // Prepare image(s)
            for i in 0..<photoAttachedIDs.count {
                let adImage = photoButtons[i].imageView!.image
                let imageData = UIImageJPEGRepresentation(adImage!, 1.0)
                let imageFile = PFFile(name:"image.jpg", data:imageData!)
                adObj["image\(i+1)"] = imageFile
            }
            
            
            // Saving...
            adObj.saveInBackground { (succ, error) in
                if error == nil {
                    self.hideHUD()
                    
                    // Store followedBy IDs for this Ad
                    let query = PFQuery(className: FOLLOW_CLASS_NAME)
                    query.whereKey(FOLLOW_IS_FOLLOWING, equalTo: currentUser)
                    query.findObjectsInBackground { (objects, error) in
                        if error == nil {
                            if objects!.count != 0 {
                                var followedBy = self.adObj[ADS_FOLLOWED_BY] as! [String]
                                
                                for i in 0..<objects!.count {
                                    // Parse Object
                                    var fObj = PFObject(className: FOLLOW_CLASS_NAME)
                                    fObj = objects![i]
                                    // User Pointer
                                    let userPointer = fObj[FOLLOW_CURRENT_USER] as! PFUser
                                    userPointer.fetchIfNeededInBackground(block: { (user, error) in
                                        if error == nil {
                                            followedBy.append(userPointer.objectId!)
                                            if i == objects!.count-1 { 
                                                self.adObj[ADS_FOLLOWED_BY] = followedBy
                                                self.adObj.saveInBackground() 
                                            }
                                        // error
                                        } else { self.simpleAlert("\(error!.localizedDescription)")
                                    }})// ./ userPointer
                                    
                                }// ./ For loop
                                
                            }// ./ If
                            
                        // error
                        } else { self.simpleAlert("\(error!.localizedDescription)")
                    }}

                    
                    // Fire alert
                    let alert = UIAlertController(title: APP_NAME,
                        message: "Congratulations, your stuff has been successfully posted!",
                        preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                        noReload = false
                        self.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    
                // error
                } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription)")
            }}
            
        }// ./ If
    }
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - MARK AS SOLD BUTTON
    // ------------------------------------------------
    @IBAction func markAsSoldButt(_ sender: Any) {
        let alert = UIAlertController(title: APP_NAME,
            message: "Are you sure you want to mark this Item as Sold? You will not be able to change it later.",
            preferredStyle: .alert)
        
        // MARK AS SOLD
        let markSold = UIAlertAction(title: "Mark as Sold", style: .default, handler: { (action) -> Void in
            self.adObj[ADS_IS_SOLD] = true
            self.adObj.saveInBackground(block: { (succ, error) in
                if error == nil {
                    
                    // Fire alert
                    let alert2 = UIAlertController(title: APP_NAME,
                        message: "Your item has been marked as Sold! People will still be able to find this item in the app until you will delete it.",
                        preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                        self.markAsSoldButton.isHidden = true
                        mustDismiss = true
                        noReload = false
                        self.dismiss(animated: true, completion: nil)
                    })
                    alert2.addAction(ok)
                    self.present(alert2, animated: true, completion: nil)
                    
                // error
                } else { self.simpleAlert("\(error!.localizedDescription)")
            }})
        })
        alert.addAction(markSold)
        
        // CANCEL BUTTON
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    
   
    
    
    // ------------------------------------------------
    // MARK: - DELETE AD BUTTON
    // ------------------------------------------------
    @IBAction func deleteButt(_ sender: Any) {
        let alert = UIAlertController(title: APP_NAME,
            message: "Are you sure you want to delete this Item?",
            preferredStyle: .alert)
        
        // Delete Ad
        let delete = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) -> Void in
            self.showHUD()
            
            self.adObj.deleteInBackground(block: { (succ, error) in
                if error == nil {
                    self.hideHUD()
                    
                    // Query and Delete Chats relative to this Ad (if any)
                    let query = PFQuery(className: CHATS_CLASS_NAME)
                    query.whereKey(CHATS_AD_POINTER, equalTo: self.adObj)
                    query.findObjectsInBackground { (objects, error) in
                        if error == nil {
                            if objects!.count != 0 {
                                for i in 0..<objects!.count {
                                    var cObj = PFObject(className: CHATS_CLASS_NAME)
                                    cObj = objects![i]
                                    cObj.deleteInBackground();
                                }// ./ For
                            }// ./ If
                            
                        // error
                        } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription)")
                    }}
                    
                    // Fire alert
                    let alert = UIAlertController(title: APP_NAME,
                        message: "Your Item has been successfully deleted!",
                        preferredStyle: .alert)
                    
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                        self.dismiss(animated: true, completion: nil)
                        mustDismiss = true
                        noReload = false
                        
                    })
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    
                // error
                } else { self.hideHUD(); self.simpleAlert("\(error!.localizedDescription)")
            }})
        })
        alert.addAction(delete)
        
        
        // Cancel
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    // ------------------------------------------------
    // MARK: - TEXT FIELD DELEGATES
    // ------------------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == adTitleTxt { adPriceTxt.becomeFirstResponder() }
    return true
    }
    
    
    
    // ------------------------------------------------
    // MARK: - DISMISS KEYBOARD
    // ------------------------------------------------
    func dismisskeyboard() {
        adTitleTxt.resignFirstResponder()
        adPriceTxt.resignFirstResponder()
        descriptionTxt.resignFirstResponder()
    }
    
    
    // ------------------------------------------------
    // MARK: - DISMISS BUTTON
    // ------------------------------------------------
    @IBAction func dismissButt(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    

    // ------------------------------------------------
    // MARK: - RESET VARIABLES ON VIEW DID DISAPPEAR
    // ------------------------------------------------
    override func viewDidDisappear(_ animated: Bool) {
        // chosenLocation = nil
    }
    
    

}// ./ end
